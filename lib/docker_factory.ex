require EEx

defmodule DockerFactory do
  @moduledoc """
  Documentation for DockerFactory.
  """

  @doc """
  Hello world.

  ## Examples

      iex> DockerFactory.hello()
      :world

  """
  def hello do
    :world
  end

    EEx.function_from_string(:def, :image_name,
        "<%= if registry do %><%= registry %>/<% end %>" <>
        "<%= name %>" <>
        "<%= if tag do %>:<%= tag %><% end %>",
        [:registry, :name, :tag]
    )

    def build_test_push(
            dockercmd,
            registry,
            image_name_short,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, testcmdlist: nil, push: nil],
            options
        )
        tag = options[:tag]
        testcmdlist = options[:testcmdlist]
        push = options[:push]

        # IO.puts(tag)
        # IO.puts(testcmdlist)
        # IO.puts(push)

        docker_image_name = image_name(
            registry,
            image_name_short,
            tag
        )

        dockerlinecmd = """
        docker build --tag #{docker_image_name} - << EOM
        #{dockercmd}
        EOM
        """

        IO.puts(dockerlinecmd)

        System.cmd("/bin/sh", ["-c", dockerlinecmd],
            into: IO.stream(:stdio, :line)
        )

        IO.puts(testcmdlist)
        if testcmdlist do # Results are ignored!
            System.cmd("docker",
                ["run", "#{docker_image_name}", "-c"] ++ testcmdlist,
                into: IO.stream(:stdio, :line)
            )
        end

        if push do
            System.cmd("docker",
                ["push", "#{docker_image_name}"],
                into: IO.stream(:stdio, :line)
            )
        end
    end

    EEx.function_from_string(:def, :apk_add,
        """
        RUN apk add --no-cache \
        <%= for item <- packages do %>\
        <%= item %> \
        <% end %>\
        """,
        [:packages]
    )

    EEx.function_from_string(:def, :apt_get_install,
        """
        RUN apt-get update --quiet=2 \
        && apt-get install --no-install-recommends --assume-yes --quiet=2 -o=Dpkg::Use-Pty=0 \
        <%= for item <- packages do %>\
        <%= item %> \
        <% end %>\
        && rm -rf /var/lib/apt/lists/* \
        """,
        [:packages]
    )

    EEx.function_from_string(:def, :dnf_install,
        """
        RUN if (! command -v dnf) then yum install --assumeyes dnf ; fi \
        && dnf install --assumeyes \
        <%= for item <- packages do %>\
        <%= item %> \
        <% end %>\
        && dnf clean all \
        """,
        [:packages]
    )

    EEx.function_from_string(:def, :microdnf_install,
        """
        RUN microdnf install --assumeyes \
        <%= for item <- packages do %>\
        <%= item %> \
        <% end %>\
        && microdnf clean all \
        """,
        [:packages]
    )

    @doc """
    Docker apt-get... && add-apt-repository... [in Dockerfile]
    
    Needs software-properties-common Debian Package
    https://packages.debian.org/search?suite=default&section=all&arch=any&searchon=contents&keywords=add-apt-repository
    """
    EEx.function_from_string(:def, :_apt_get_install_add_repository,
        """
        RUN apt-get update --quiet=2 \
            && apt-get install --no-install-recommends --assume-yes --quiet=2 -o=Dpkg::Use-Pty=0 \
                <%= for item <- packages do %><%= item %> <% end %> \
            <%= for item <- repositories do %>&& add-apt-repository '<%= item %>' <% end %> \
            && rm -rf /var/lib/apt/lists/* \
        """,
        [:packages, :repositories]
    )
    # Working
    # software-properties-common is heavy for a Docker image! 
    # https://stackoverflow.com/questions/28459493/iterate-over-list-in-embedded-elixir
    # Be sure and certain that apt_sources_list replacing this function
    # Is perfectly working before to remove this.
    # The idea could also be moved to
    # software-properties-common Debian package notes

    @doc """
    Docker RUN add-apt-repository... in Dockerfile
    
    Needs software-properties-common Debian Package
    https://packages.debian.org/search?suite=default&section=all&arch=any&searchon=contents&keywords=add-apt-repository
    """
    EEx.function_from_string(:def, :_run_add_apt_repository,
        """
        <%= for item <- repositories do %>
        RUN add-apt-repository '<%= item %>'
        <% end %>
        """,
        [:repositories]
    )
    # software-properties-common is heavy for a Docker image! 

    @doc """
    [Docker RUN ...] && add-apt-repository... [in Dockerfile]
    
    Needs software-properties-common Debian Package
    https://packages.debian.org/search?suite=default&section=all&arch=any&searchon=contents&keywords=add-apt-repository
    """
    EEx.function_from_string(:def, :_and_add_apt_repository,
        "<%= for item <- repositories do %>&& add-apt-repository '<%= item %>' <% end %>",
        [:repositories]
    )

    EEx.function_from_string(:def, :apk_repositories,
        """
        <%= for item <- repositories do %>
        RUN echo '<%= item %>' >> /etc/apk/repositories
        <% end %>
        """,
        [:repositories]
    )

    EEx.function_from_string(:def, :apt_sources_list,
        """
        <%= for item <- repositories do %>
        RUN echo '<%= item %>' >> /etc/apt/sources.list
        <% end %>
        """,
        [:repositories]
    )

    @php_init """
        ENV COMPOSER_NO_INTERACTION 1
        """

    @sudo_init_user_busybox """
        && adduser -D user \
        && echo "user ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/user
        ENV CHRONIC chronic
        ENV SUDO sudo
        ENTRYPOINT ["/bin/su", "user"]
        """

    @sudo_init_user_debian """
        && adduser --disabled-password --quiet user \
        && echo "user ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/user
        ENV CHRONIC chronic
        ENV SHELL /bin/bash
        ENV SUDO sudo
        ENV TERM xterm-color
        ENTRYPOINT ["/bin/bash", "-c", "command -v cat ; command -v script ; printenv TERM ; (cat|cat) | (sleep 3 ; cat) | (runuser -u user -- script --return --flush --quiet --command /bin/bash /dev/null)"]
        """
        # "runuser", "-u", "user", "--", 
        # "script", "--force", "--command", "bash", "/dev/stdout"
        # "--quiet",
        # ( sleep 3 ; cat | cat) | (...

    @sudo_init_user_redhat """
        && adduser user \
        && echo "user ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/user
        ENV CHRONIC chronic
        ENV SUDO sudo
        ENTRYPOINT ["/bin/su", "user"]
        """

    def apk_init_and_sudo(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [
                tag: nil,
                packages: [
                    "moreutils",
                    "sudo"
                ],
                push: nil,
                repositories_to_add: []
            ],
            options
        )
        tag = options[:tag]
        packages = options[:packages]
        push = options[:push]
        repositories_to_add = options[:repositories_to_add]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            @php_init <>
            apk_repositories(repositories_to_add) <>
            apk_add(packages) <>
            @sudo_init_user_busybox,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami"],
            push: push
        )
    end

    def apt_init_and_sudo(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [
                tag: nil,
                packages: [
                    "bsdutils", # /usr/bin/script
                    "coreutils", # /bin/cat
                    "moreutils",
                    "sudo"
                ],
                push: nil,
                repositories_to_add: []
            ],
            options
        )
        tag = options[:tag]
        packages = options[:packages]
        push = options[:push]
        repositories_to_add = options[:repositories_to_add]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        IO.puts("ls /var/lib/apt/lists")
        System.cmd("docker", # Debian only
            ["run", from_image, "ls", "--color=always", "/var/lib/apt/lists"],
            into: IO.stream(:stdio, :line)
        )

        IO.puts("ls /var/cache/apt")
        System.cmd("docker", # Debian only
            ["run", from_image, "ls", "--color=always", "/var/cache/apt"],
            into: IO.stream(:stdio, :line)
        )

        IO.puts("apt-cache policy sudo")
        System.cmd("docker", # Debian only
            ["run", from_image, "sh", "-c", "apt-get update --quiet=2 && apt-cache policy sudo"],
            into: IO.stream(:stdio, :line)
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            @php_init <>
            """
            RUN echo 'APT::Color "1";' > /etc/apt/apt.conf.d/99color
            RUN echo 'Dpkg::Use-Pty "0";' > /etc/apt/apt.conf.d/99use-pty
            ENV DEBIAN_FRONTEND noninteractive
            ENV DPKG_COLORS always
            RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
            """ <>
            apt_sources_list(repositories_to_add) <>
            apt_get_install(packages) <>
            @sudo_init_user_debian,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami"],
            push: push
        )

        IO.puts("ls /var/lib/apt/lists")
        System.cmd("docker", # Debian only
            ["run", "#{to_image}", "-c", "ls --color=always /var/lib/apt/lists"],
            into: IO.stream(:stdio, :line)
        )

        IO.puts("ls /var/cache/apt/archives")
        System.cmd("docker", # Debian only
            ["run", "#{to_image}", "-c", "ls --color=always /var/cache/apt/archives"],
            into: IO.stream(:stdio, :line)
        )

        IO.puts("cat /etc/apt/sources.list")
        System.cmd("docker", # Debian only
            ["run", "#{to_image}", "-c", "cat /etc/apt/sources.list"],
            into: IO.stream(:stdio, :line)
        )
    end
    # --quiet=2 implies --assume-yes # Only with apt-get and not with aptitude.
    # In aptitude, --quiet=n, n can maybe be greater than 2.
    # Dpkg::Use-Pty=0 could be placed in apt configuration.


    def dnf_init_and_sudo(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [
                tag: nil,
                packages: [
                    "moreutils",
                    "sudo"
                ],
                push: nil,
                repositories_to_add: []
            ],
            options
        )
        tag = options[:tag]
        packages = options[:packages]
        push = options[:push]
        _repositories_to_add = options[:repositories_to_add]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            @php_init <>
            """
            """ <>
            dnf_install(packages) <>
            @sudo_init_user_redhat,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami"],
            push: push
        )
    end


    def microdnf_init_and_sudo(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [
                tag: nil,
                packages: [
                    "moreutils",
                    "sudo"
                ],
                push: nil,
                repositories_to_add: []
            ],
            options
        )
        tag = options[:tag]
        packages = options[:packages]
        push = options[:push]
        _repositories_to_add = options[:repositories_to_add]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            @php_init <>
            """
            """ <>
            microdnf_install(packages) <>
            @sudo_init_user_redhat,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami"],
            push: push
        )
    end


    def apk_flatpak(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apk_add([
                "flatpak",
                "gsettings-desktop-schemas"
            ]),

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; flatpak --version"],
            push: push
        )
    end

    def apt_flatpak(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apt_get_install([
                "ca-certificates",
                "flatpak"
            ]),

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; flatpak --version"],
            push: push
        )
    end


    @openrc_init """
    && /sbin/openrc sysinit \
    && /sbin/openrc boot \
    && /sbin/openrc default \
    && rc-status \
    && openrc shutdown
    ENTRYPOINT ["/bin/sh", "-c", "trap 'openrc shutdown' 0 ; openrc ; su user \\\"\\\$@\\\"", "--"]
    """
    # set -o xtrace ;
    # ENTRYPOINT ["/bin/sh", "-c", "set -e -o xtrace ; openrc ; su user \\\"\\\$@\\\" ; status=$? ; openrc shutdown ; echo Ready for shutdown with status $status. ; exit $status", "--"]

    def apk_openrc(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apk_add([
                "busybox-initscripts",
                "openrc"
            ]) <>
            @openrc_init,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; rc-status"],
            push: push
        )
    end

    def apt_openrc(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apt_get_install([
                "initscripts",
                "openrc"
            ]) <>
            @openrc_init,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; rc-status"],
            push: push
        )
    end

    @rc_update_add_mariadb """
    && rc-update add mariadb default \
    && rc-service mariadb setup \
    """

    # @mariadb_create_user_db """
    # && openrc \
    # && echo "CREATE USER IF NOT EXISTS 'database_user'@'localhost' \
    # IDENTIFIED BY 'user_password';" | mysql --verbose \
    # && echo "CREATE DATABASE IF NOT EXISTS user_database;" | mysql --verbose \
    # && echo "GRANT ALL PRIVILEGES ON user_database.* \
    # TO 'database_user'@'localhost';" | mysql --verbose \
    # && openrc shutdown
    # ENV DATABASE_URL mysql://database_user:user_password@localhost/user_database
    # """
    @mariadb_create_user_db """
    && openrc \
    && echo "CREATE USER IF NOT EXISTS 'user'@'localhost' \
    IDENTIFIED BY 'user';" | mysql --verbose \
    && echo "CREATE DATABASE IF NOT EXISTS user;" | mysql --verbose \
    && echo "GRANT ALL PRIVILEGES ON user.* \
    TO 'user'@'localhost';" | mysql --verbose \
    && openrc shutdown
    ENV DATABASE_URL mysql://user:user@localhost/user
    """
    # ENV DATABASE_URL mysql://user:secret@localhost/mydb

    def apk_mariadb(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apk_add([
                "mariadb",
                "mariadb-client"
            ]) <>
            @rc_update_add_mariadb <>
            @mariadb_create_user_db,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; rc-status"],
            push: push
        )
    end

    def apt_mariadb(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apt_get_install([
                "mariadb-server",
                "mariadb-client"
            ]) <>
            @mariadb_create_user_db,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; rc-status"],
            push: push
        )
    end

    @rc_update_add_postgresql """
    && rc-update add postgresql default \
    """

    @postgresql_create_user_db """
    && openrc \
    && su postgres -c 'printf "user\\nuser" | createuser --pwprompt user' \
    && su postgres -c 'createdb -O user user' \
    && openrc shutdown
    """
    # ENV DATABASE_URL postgresql://...
    # createuser -D/--no-createdb -R/--no-createrole -S/--no-superuser # are default.

    def apk_postgresql(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apk_add([
                "postgresql",
                "postgresql-client"
            ]) <>
            @rc_update_add_postgresql <>
            @postgresql_create_user_db,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; rc-status"],
            push: push
        )
    end

    def apt_postgresql(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apt_get_install([
                "postgresql",
                "postgresql-client"
            ]) <>
            @postgresql_create_user_db,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; rc-status"],
            push: push
        )
    end

    def apk_sqlite(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apk_add(["sqlite"]),

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; rc-status"],
            push: push
        )
    end

    def apt_sqlite(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apt_get_install(["sqlite3"]),

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; rc-status"],
            push: push
        )
    end

    @common_php_most_packages [
        "composer",
        "git",
        "php-curl",
        "php-gd",
        "php-mbstring",
        "php-xml",
        "php-zip",
        "unzip"
    ]


    @apt_php_most_packages @common_php_most_packages ++ [
        "php-cli"
    ]

    @apk_php_most_packages @common_php_most_packages ++ [
        "php",
        "php-ctype",
        "php-dom",
        "php-iconv",
        "php-json",
        "php-openssl",
        "php-pcntl", # For symfony/web-server-bundle
        "php-phar",
        "php-posix", # For symfony/web-server-bundle
        "php-session",
        "php-simplexml",
        "php-tokenizer",
        "php-xmlwriter"
    ]

    @apk_php_timezone """
    && sed --in-place='~' -- 's#^;date.timezone =\\$#date.timezone = America/New_York#g' /etc/php*/php.ini
    """

    @apt_php_timezone """
    && sed --in-place='~' -- 's#^;date.timezone =\\$#date.timezone = America/New_York#g' /etc/php/*/cli/php.ini
    """

    @php_mysql_env """
    ENV DB_TYPE mysql
    ENV DB_USER user
    ENV DB_PASS user
    ENV DB_HOST localhost
    ENV DB_NAME user
    """
    # ENV DB_PORT 

    def apk_php_pdo_mysql(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apk_add(["php-pdo_mysql"] ++ @apk_php_most_packages) <>
            @apk_php_timezone <>
            @php_mysql_env,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; rc-status"],
            push: push
        )
    end

    # def apt_php_pdo_mysql(
        # Debian packge name changed.
    def apt_php_mysql(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apt_get_install(["php-mysql"] ++ @apt_php_most_packages) <>
            @apt_php_timezone <>
            @php_mysql_env,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; rc-status"],
            push: push
        )
    end

    @php_pgsql_env """
    
    ENV DATABASE_URL postgresql://user:user@localhost:5432/user
    ENV DB_TYPE pgsql
    ENV DB_USER user
    ENV DB_HOST /var/run/postgresql
    ENV DB_PORT 5432
    ENV DB_NAME user
    """
    # ENV DB_PASS user

    def apk_php_pdo_pgsql(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apk_add(["php-pdo_pgsql"] ++ @apk_php_most_packages) <>
            @apk_php_timezone <>
            @php_pgsql_env,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; rc-status"],
            push: push
        )
    end

    # def apt_php_pdo_pgsql(
        # Name change in Debian virtual package.
    def apt_php_pgsql(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apt_get_install(["php-pgsql"] ++ @apt_php_most_packages) <>
            @apt_php_timezone <>
            @php_pgsql_env,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; rc-status"],
            push: push
        )
    end

    @php_sqlite_env """
    
    ENV DATABASE_URL sqlite:////home/user/sqlite_data.db
    ENV DB_TYPE sqlite
    """

    def apk_php_pdo_sqlite(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apk_add(["php-pdo_sqlite"] ++ @apk_php_most_packages) <>
            @apk_php_timezone <>
            @php_sqlite_env,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; rc-status"],
            push: push
        )
    end

    # def apt_php_pdo_sqlite(
        # Name change in Debian virtual package.
    def apt_php_sqlite3(
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(
            """
            FROM #{from_image}
            """ <>
            apt_get_install(["php-sqlite3"] ++ @apt_php_most_packages) <>
            @apt_php_timezone <>
            @php_sqlite_env,

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami; rc-status"],
            push: push
        )
    end

    def make_alpine_images(
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, repositories_to_add: []],
            options
        )
        tag = options[:tag]
        repositories_to_add = options[:repositories_to_add]
        ci_registry_image = System.fetch_env!("CI_REGISTRY_IMAGE")

        # alpine/sudo
        apk_init_and_sudo(
            nil, "alpine",
            ci_registry_image, "sudo",
            tag: tag,
            push: true,
            repositories_to_add: repositories_to_add
        )

        # alpine/flatpak
        apk_flatpak(
            ci_registry_image, "sudo",
            ci_registry_image, "flatpak",
            tag: tag,
            push: true
        )

        # alpine/openrc
        apk_openrc(
            ci_registry_image, "sudo",
            ci_registry_image, "openrc",
            tag: tag,
            push: true
        )

        # alpine/mariadb
        apk_mariadb(
            ci_registry_image, "openrc",
            ci_registry_image, "mariadb",
            tag: tag,
            push: true
        )

        # alpine/postgresql
        apk_postgresql(
            ci_registry_image, "openrc",
            ci_registry_image, "postgresql",
            tag: tag,
            push: true
        )

        # alpine/sqlite
        apk_sqlite(
            ci_registry_image, "openrc", #!
            ci_registry_image, "sqlite",
            tag: tag,
            push: true
        )

        # alpine/php-pdo_mysql
        apk_php_pdo_mysql(
            ci_registry_image, "mariadb",
            ci_registry_image, "php-pdo_mysql",
            tag: tag,
            push: true
        )

        # alpine/php-pdo_pgsql
        apk_php_pdo_pgsql(
            ci_registry_image, "postgresql",
            ci_registry_image, "php-pdo_pgsql",
            tag: tag,
            push: true
        )

        # alpine/php-pdo_sqlite
        apk_php_pdo_sqlite(
            ci_registry_image, "sqlite",
            ci_registry_image, "php-pdo_sqlite",
            tag: tag,
            push: true
        )
    end

    def make_debian_images(
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, repositories_to_add: []],
            options
        )
        tag = options[:tag]
        repositories_to_add = options[:repositories_to_add]
        ci_registry_image = System.fetch_env!("CI_REGISTRY_IMAGE")

        # debian/sudo
        apt_init_and_sudo(
            nil, "debian",
            ci_registry_image, "sudo",
            tag: tag,
            push: true,
            repositories_to_add: repositories_to_add
        )

        # debian/flatpak
        apt_flatpak(
            ci_registry_image, "sudo",
            ci_registry_image, "flatpak",
            tag: tag,
            push: true
        )

        # debian/openrc
        apt_openrc(
            ci_registry_image, "sudo",
            ci_registry_image, "openrc",
            tag: tag,
            push: true
        )

        # debian/mariadb
        apt_mariadb(
            ci_registry_image, "openrc",
            ci_registry_image, "mariadb",
            tag: tag,
            push: true
        )

        # debian/postgresql
        apt_postgresql(
            ci_registry_image, "openrc",
            ci_registry_image, "postgresql",
            tag: tag,
            push: true
        )

        # debian/sqlite
        apt_sqlite(
            ci_registry_image, "openrc", #!
            ci_registry_image, "sqlite",
            tag: tag,
            push: true
        )

        # debian/php-mysql
        apt_php_mysql(
            ci_registry_image, "mariadb",
            ci_registry_image, "php-mysql",
            tag: tag,
            push: true
        )

        # debian/php-pgsql
        apt_php_pgsql(
            ci_registry_image, "postgresql",
            ci_registry_image, "php-pgsql",
            tag: tag,
            push: true
        )

        # debian/php-sqlite
        apt_php_sqlite3(
            ci_registry_image, "sqlite",
            ci_registry_image, "php-sqlite3",
            tag: tag,
            push: true
        )
    end

    def make_dnf_images(
            from_image,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, repositories_to_add: []],
            options
        )
        tag = options[:tag]
        # repositories_to_add = options[:repositories_to_add]
        ci_registry_image = System.fetch_env!("CI_REGISTRY_IMAGE")

        # dnf/sudo
        dnf_init_and_sudo(
            nil, from_image,
            ci_registry_image, "sudo",
            tag: tag,
            push: true
            # repositories_to_add: repositories_to_add
        )
    end

    def make_microdnf_images(
            from_image,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, repositories_to_add: []],
            options
        )
        tag = options[:tag]
        # repositories_to_add = options[:repositories_to_add]
        ci_registry_image = System.fetch_env!("CI_REGISTRY_IMAGE")

        # microdnf/sudo
        microdnf_init_and_sudo(
            nil, from_image,
            ci_registry_image, "sudo",
            tag: tag,
            push: true
            # repositories_to_add: repositories_to_add
        )
    end

    def make_ubuntu_images(
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, repositories_to_add: []],
            options
        )
        tag = options[:tag]
        repositories_to_add = options[:repositories_to_add]
        ci_registry_image = System.fetch_env!("CI_REGISTRY_IMAGE")

        # ubuntu/sudo same as for Debian
        apt_init_and_sudo(
            nil, "ubuntu",
            ci_registry_image, "sudo",
            tag: tag,
            push: true,
            repositories_to_add: repositories_to_add
        )

        # ubuntu/flatpak same as for Debian
        apt_flatpak(
            ci_registry_image, "sudo",
            ci_registry_image, "flatpak",
            tag: tag,
            push: true
        )

        # ubuntu/openrc # openrc from Debian package
        apt_openrc(
            ci_registry_image, "sudo",
            ci_registry_image, "openrc",
            tag: tag,
            push: true
        )

        # ubuntu/sqlite
        apt_sqlite(
            ci_registry_image, "sudo", #!
            ci_registry_image, "sqlite",
            tag: tag,
            push: true
        )

        # ubuntu/php-sqlite
        apt_php_sqlite3(
            ci_registry_image, "sqlite",
            ci_registry_image, "php-sqlite3",
            tag: tag,
            push: true
        )
    end

    def image_from_apt_get_install( # Unused?
            from_registry,
            from_name,
            to_registry,
            to_name,
            options \\ []
        ) do
        options = Keyword.merge(
            [tag: nil, push: nil],
            options
        )
        tag = options[:tag]
        push = options[:push]
        from_image = image_name(
            from_registry,
            from_name,
            tag
        )
        _to_image = image_name(
            to_registry,
            to_name,
            tag
        )

        build_test_push(EEx.eval_string(
            """
            FROM #{from_image}
            """ <>
            apt_get_install([
                "hello"
            ]) <>
            """
            ENTRYPOINT ["/bin/su", "user"]
            """
            ), # Entrypoint?

            to_registry,
            to_name,
            tag: tag,
            testcmdlist: ["whoami"],
            push: push
        )
        # ? ENTRYPOINT ["/bin/su", "user"]?
    end
end

# add-apt-repository 'deb http://ftp.debian.org/debian stable main contrib non-free' \
# software-properties-common is heavy for a Docker image! 
# TODO
# https://google.com/search?q=debian+add+repository
# How To Add Apt Repository In Ubuntu & Debian
#   https://tecadmin.net/add-apt-repository-ubuntu/

# https://elixirforum.com/t/is-there-a-way-to-do-default-named-parameters/18927/5
# https://hexdocs.pm/elixir/System.html
