# docker_factory

Elixir scripts to produce Dockerfiles and Docker images

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `docker_factory` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:docker_factory, git: "https://gitlab.com/docker-images4automated-tests/docker_factory.git", tag: "0.1.0-dev"}
    # {:docker_factory, "~> 0.1.0-dev"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/docker_factory](https://hexdocs.pm/docker_factory).

## Proposed dependencies not used yet
* [hadolint](http://hackage.haskell.org/package/hadolint)
  * [repology](https://repology.org/projects/?search=hadolint)